export interface IImage {
	public_id: string,
	version: number,
	signature: string,
	width: number,
	height: number,
	format: string,
	resource_type: string,
	created_at: string,
	tags: {},
	bytes: string,
	type: string,
	etag: string,
	placeholder: boolean,
	url: string,
	secure_url: string,
	info: {},
	original_filename: string
}