import {IUser} from './../user';
import {IImage} from './../user';

export interface IStory {
	user_id: IUser.id,
	image: IImage
}