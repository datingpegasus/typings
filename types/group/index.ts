import {IUser, UserSchema, userTestData} from '../user';

export interface IGroup {
    id?: string;
    name: string;
    description: string;
    createdAt: string;
    updatedAt: string;
}
export const GroupSchema = {
    name: {type: String, required: true},
    description: {type: String},
    createdAt: {type: Date, default: Date.now},
    updatedAt: {type: Date, default: Date.now},
};
export interface IGroupElection {
    id?: string;
    group: IGroup,
    user: IUser,
    started: string,
    ended: string
    createdAt?: string;
    updatedAt?: string;
}
export const GroupElectionSchema = {
    group: GroupSchema,
    user: UserSchema,
    startedAt: {type: Date, default: Date.now},
    endedAt: {type: Date, required: false},
    createdAt: {type: Date, default: Date.now},
    updatedAt: {type: Date, default: Date.now},
};
export interface IGroupElectionVote {
    id?: string;
    electionId: string,
    voteUp: boolean,
    user: IUser
    createdAt?: string,
    updatedAt?: string
}
export const GroupElectionVoteSchema = {
    electionId: {type: String, required: true},
    voteUp: {type: Boolean, required: true},
    user: UserSchema,
    createdAt: {type: Date, default: Date.now},
    updatedAt: {type: Date, default: Date.now},
};

export const groupData = {
    name: 'elite',
    description: 'this is test group'
};

export const electionGroupTestData = {
    group: groupData,
    user: userTestData,
};

export const electionVoteTestData = {
    voteUp: true,
    user: userTestData,
    electionId: '507f191e810c19729de860ea'
};
