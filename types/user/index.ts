import {IMessage} from '../message';

export interface IUserIsTyping {
    senderId: string;
    conversationId: string;
    receiverId: string;
}

export interface IMessageDelivered {
    message: IMessage;
    delivered: boolean;
}

export interface IUserConnectedToConversation {
    conversationId: string;
    userId: string;
}

export interface IUser {
    id: string,
    email: string,
    firstName: string,
    lastName: string,
    verified: boolean,
    createdAt: Date,
    updatedAt: Date
}
