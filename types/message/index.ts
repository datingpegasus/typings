//CODE 1 - bad send message
import {IUser} from '../user'

export interface IMessage {
    id?: string;
    conversationId?: string;
    senderId: string;
    receiverId: string;
    message: string;
    type: number;
    attachmentUrl?: string | null;
    delivered?: boolean;
    sent: string | number;
    createdAt?: string | number;
    updatedAt?: string | number;
}

export interface IStorageMessage {
    id: string;
    conversationId: string;
    senderId: string;
    receiverId: string;
    message: string;
    type: number;
    attachmentUrl?: string | null;
    delivered: boolean;
    sent: string;
    createdAt: string;
    updatedAt: string;
    profanity: {
        censor: string,
        offensive: boolean
    },
    sender?: IUser,
    receiver?: IUser,
}
export interface IInitConversationMessages {
    userId: string,
    messages: IStorageMessage[]
}
export interface IBackendMessage {
    successful: boolean;
    message: IMessage;
}

export interface IUserConversations {
    conversations: IMessage[],
    userId: string
}
