import {IUser} from './../user';

export interface IFeed {
    id?: number,
    avatar: string,
    author: string,
    time: string,
    image: string,
    likeCount?: number,
    commentCount: number,
    shareCount: number,
    videoUrl: string,
    text: string,
    isImage: boolean,
    isVideo: boolean,
    isText: boolean,
    createdAt?: string;
    updatedAt?: string;
    profanity: {
        censor: string,
        offensive: boolean
    },
    user: IUser,
    comments: IComment[]
}
export interface IFeedBeforeSave {
    id?: number,
    avatar: string,
    author: string,
    time: string,
    image: string,
    likeCount?: number,
    commentCount: number,
    shareCount: number,
    videoUrl: string,
    text: string,
    isImage: boolean,
    isVideo: boolean,
    isText: boolean,
    user: IUser,
}
export interface IComment {
    id?: string;
    feedId: string;
    message: string;
    type: number;
    attachmentUrl?: string | null;
    sent: string;
    createdAt?: string;
    updatedAt?: string;
    profanity: {
        censor: string,
        offensive: boolean
    },
    user: IUser,
}

export interface IAllFeeds {
    feeds: IFeed[],
    userId: string
}

export interface IFeedLike {
    id?: string;
    feedId: string;
    sent: string;
    createdAt?: string;
    updatedAt?: string;
    user: IUser
}

export interface ICommentBeforeSave {
    id?: string;
    feedId: string;
    message: string;
    type: number;
    attachmentUrl?: string | null;
    sent: string;
    createdAt?: string;
    updatedAt?: string;
    user: IUser
}

export interface ICommentsForFeed {
    comments: IComment[],
    userId: number
}
